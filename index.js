const path = require("path");
const fs = require("fs");
const MediaSplit = require("media-split");
const argv = require("minimist")(process.argv.slice(2));
const filenames = argv._;

filenames.forEach((filename) => {
    console.log("Splitting", filename);

    const input = path.resolve(process.cwd(), "input/", filename + ".mp3");
    const times = fs.readFileSync(path.resolve(process.cwd(), "input/", filename + ".txt")).toString();
    const output = path.resolve(process.cwd(), "output/");
    const sections = times.split(/\n/).filter((l) => !/^\s*$/.test(l));

    const split = new MediaSplit({
        input,
        sections,
        output,
        concurrency: 5,
    });

    split.parse().then((sections) => {
        sections.forEach((section) => console.log("Wrote", section.name));
    });
});
